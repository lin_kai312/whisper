#from pathlib import Path
import sys
import os
import winreg
import yaml
import pprint


class 基础:
    def __init__(self):
        self.软件路径 = ''
        self.功能列表 = []
        self.KeyList = [winreg.HKEY_LOCAL_MACHINE,winreg.HKEY_CURRENT_USER]
        self.scriptKeyPath = 'Software\Microsoft\Windows\CurrentVersion\Run'
        self.基础配置 = ''

    def 获取软件路径(self):
        if getattr(sys, 'frozen', False):
            软件目录 = os.path.dirname(sys.executable)
        elif __file__:
            软件目录 = os.path.dirname(os.path.abspath(__file__))
        self.软件目录 = 软件目录
        print("软件目录", 软件目录)
        return 软件目录

    def 是不是整数(self, 字符):  # 是否为整数，用于判断版本
        print("判断的字符", 字符)
        判断结果 = False
        if str.isdigit(字符):
            临时字符 = int(字符)
            if 临时字符 <= len(self.功能列表):
                判断结果 = True
        else:
            print("信息: 数据非整数")
        print("信息 数据识别结果", 判断结果)
        return 判断结果
  
    def IsAutoStart(self):
        ASState = False
        for key in self.KeyList:
            try:
                ASStateReg = winreg.CreateKey(key,self.scriptKeyPath)
            except WindowsError as e:
                print("INFO 02: AutoStart %s"%str(key),e)
            else:
                if (not ASState):
                    try:
                        value = winreg.QueryValueEx(ASStateReg, 'WhisperHope')
                        print("INFO 03: AutoStart %s"%str(key),value)
                        winreg.CloseKey(ASStateReg)
                        ASState = True
                    except WindowsError as e:
                        winreg.CloseKey(ASStateReg)
                        print("INFO 04: AutoStart %s"%str(key),e)   
        return ASState
        
    def 设置自启动(self,软件名称):
        ASState = False
        for key in self.KeyList:
            try:
                CreatKey = winreg.CreateKey(key,self.scriptKeyPath)
                CreatResult = winreg.SetValueEx(CreatKey, 'WhisperHope', 0, winreg.REG_SZ, '''"{}"'''.format(sys.argv[0]))
                winreg.CloseKey(CreatResult)
            except :
                print(sys.exc_info()[0])
            else:
                ASState = self.IsAutoStart()
                if ASState:
                    break
        ASState = self.IsAutoStart()
        return ASState


    def 加载配置文档(self):
        配置文档路径 = ("{0}\\WhisperHope.yaml".format(self.获取软件路径()))
        print(配置文档路径)
        if not os.path.exists(配置文档路径):
            with open(配置文档路径, 'w', encoding='utf-8') as 配置文档注入:
                配置文档注入.write('未设置')
                配置文档注入.close()
        while True:     
            with open(配置文档路径, encoding='utf-8') as 临时数据:
                self.基础配置 = yaml.safe_load(临时数据)
                if self.基础配置 == '未设置' or self.基础配置 == '':
                    os.system("notepad.exe {0}".format(配置文档路径))
                else:
                    break
        pprint.pprint(self.基础配置)
        return self.基础配置
