# IOT
# pip3 install -i https://pypi.doubanio.com/simple paho-mqtt
from cv2 import split
import paho.mqtt.client as mqtt
import time
import queue
#import os
import re
import sys
import subprocess


class IOTControl:#MQtt加持
    def __init__(self,配置信息):
        super(IOTControl, self).__init__()
        print("初始化MQTT服务")
        self.ReH = ['none']
        self.ID, self.密码, self.话题, self.服务器地址 = 配置信息['MQTT'].values()
        self.命令队列 = queue.Queue()  # 创建命令队列
        self.设备名 =subprocess.Popen("hostname", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.设备名 =  self.设备名.stdout.read().decode()
        # self.设备名.read()  # time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))
        self.client = mqtt.Client(self.设备名)  # ClientId不能重复，所以使用当前时间
        self.client.username_pw_set(self.ID, self.密码)  # 必须设置，否则会返回「Connected with result code 4」，就是错误嘛
        self.连接状态 = 1  # 0表示已连接，1表示未连接
        print("INFO MQTT 02: ", "设置MQTT客户端")
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.connect(self.服务器地址, 1883, 60)
        #client.loop_forever()
        #self.client.loop_start()
    
    def re_connect(self):
        # self.client.connect(self.IOT_Address, 1883, 60)
        print("服务器连接状态", self.连接状态)
        if self.连接状态:
            print("连接MQTT服务")
            self.client.loop_start()
        
    def on_connect(self,client,userdata, flags, rc):
        self.state = ("Connected with result code "+str(rc))
        print("INFO MQTT 06: ","服务连接结果",self.state)#打印连接状态
        client.subscribe(self.话题)#订阅的主题
        print("INFO MQTT 07: ", "订阅的主题: ", self.话题)
        self.connect_state = rc
        #return 

    def on_message(self,client, userdata, msg):
        self.resi =("%s"%((msg.payload).decode('utf-8')))
        print("INFO MQTT >>> ",self.resi)
        if self.ReH[0] == 'wait':#等待长命令
            print("INFO CMD 00: ","等待参数")
            self.ReH.append(self.resi)
            if len(self.ReH) == 3:
                self.resi = ('{0},{1}'.format(self.ReH[1],self.ReH[2]))
                self.ReH = ['none']
        else:
            self.ReH[0] = self.resi
        self.recmd = ((self.resi.split(',')))#将数据处理成元组（列表）（调用的函数，函数所需参数）
        if len(self.recmd) == 2:
            print("INFO CMD 01: 添加命令 ",str(self.recmd))
            self.命令队列.put(self.recmd)#添加命令

    def 发布信息(self,消息):
        # msg = msg.encode("utf-8")
        print("发送消息",消息)
        if len(str(消息)) < 100:
            发送结果 = self.client.publish(self.话题, 消息)#发送信息
        if len(str(消息)) >= 100:
            消息切段 = re.findall(r'.{%d}'%((int(len(str(消息))))/2), 消息)
            # 消息切段 = 消息.split('',int(len(str(消息))))
            for 消息片段 in 消息切段:
                print(消息片段)
                发送结果 = self.client.publish(self.话题, 消息片段)
                time.sleep(1)
        print("消息发送结果", 发送结果)
        if 发送结果[0] == 0:
            print("    Send {} to 话题 {}".format(消息,self.话题))
        else:
            print("    Failed to send message to 话题 {}".format(self.话题))
    
    def 断开连接(self):
        self.client.disconnect()
    
    def 退出(self):
        self.断开连接()
        sys.exit(0)
           


if __name__ == '__main__':
    from base import 基础
    测试 = 基础()
    测试.加载配置文档()
    Com = IOTControl(测试.基础配置)
    Com.re_connect()
    Com.发布信息('长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试长度测试')
    for i in range(0,10):
        time.sleep(1)
    Com.退出()
        #pass


