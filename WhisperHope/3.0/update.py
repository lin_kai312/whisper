from urllib.request import Request,urlretrieve
from urllib import error
from urllib.request import urlopen
import requests
#import urllib3
import time
import json
import os
import sys
import argparse#用来获取命令参数
import base

class Update:
    def __init__(self):
        获取配置 = base.基础().加载配置文档()
        Token =  获取配置['更新']['Token']
        请求链接 = 获取配置['更新']['请求链接']
        self.请求链接 = ("{0}{1}".format(请求链接,Token))
        self.headers= {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'
            }
        服务器返回信息 = Request(self.请求链接,headers=self.headers)
        服务器返回信息 = urlopen(服务器返回信息).read()
        self.服务器返回信息 = json.loads(服务器返回信息.decode())
        
    def 云端版本(self):
        云端版本号 = self.服务器返回信息['tag_name']
        return 云端版本号

    def 下载链接(self,need):  # 获取文件链接
        self.软件链接列表 = []
        self.软件名称列表 = []
        self.文件链接列表 = []
        print(self.服务器返回信息['assets'])
        for 云_Json in (self.服务器返回信息['assets'][0:-1]):
            print("每条json",云_Json)
            软件链接 = 云_Json['browser_download_url']
            软件名称 = 云_Json['name']
            文件链接 = (urlopen(软件链接).geturl())
            print("下载地址",文件链接)
            self.软件链接列表.append(软件链接)
            self.软件名称列表.append(软件名称)
            self.文件链接列表.append(文件链接)
            time.sleep(3)  # 请求过快导致503(服务器拒绝)
                
    def 下载进度条(self, cur, total=100):
        percent = '{:.2%}'.format(cur / total)
        sys.stdout.write('\r')
        # sys.stdout.write("[%-50s] %s" % ('=' * int(math.floor(cur * 50 / total)),percent))
        sys.stdout.write("[%-100s] %s" % ('=' * int(cur), percent))
        sys.stdout.flush()


    def 下载进度(self,blocknum,blocksize,totalsize):
        """
        blocknum:当前已经下载的块
        blocksize:每次传输的块大小
        totalsize:网页文件总大小
        """
        if totalsize == 0:
            percent = 0
        else:
            percent = blocknum * blocksize / totalsize
        if percent > 1.0:
            percent = 1.0
        percent = percent * 100
        print("download : %.2f%%" %(percent))
        self.下载进度条(percent)
    
    def 下载文件(self):
        是否下载 = True  # False
        self.已下载文件 = []
        if len(self.软件名称列表) == len(self.文件链接列表) and 是否下载 == True :
            for i in range (0, len(self.软件链接列表)):
                print("第",i+1,"个",
                    "\n文件名称", self.软件名称列表[i],
                    "\n文件请求地址", self.软件链接列表[i],
                    "\n文件下载地址", self.文件链接列表[i])
                try:
                    urlretrieve(reporthook = self.下载进度, url = "%s"%self.文件链接列表[i], filename=(self.mainFilePath+self.软件名称列表[i]))
                    self.已下载文件.append(self.软件名称列表[i])
                except IOError as e:
                    print(1, e)
                except Exception as e:
                    print(2, e)
                else:
                    print(self.软件名称列表[i], "下载完成")
                time.sleep(3)
            print("全部软件已更新")
        else:
            print("\n===得到的参数异常===\n下载设置为:",是否下载)
        return self.已下载文件

    
    def ChangeFile(self):
        print("开始更新")
        l = []
        if self.已下载 != l:
            s = os.system('copy /y {0}\\resouse\\* {0}'.format(application_path))
            if os.path.isfile('{0}\\Setting.anexe'.format(self.application_path)):
            	os.remove('{0}\\Setting.exe'.format(self.application_path))
            	print("INFO SYSTEM 022: 替换更新程序")
            	os.rename('{0}\\Setting.anexe'.format(self.application_path),'{0}\\Setting.exe'.format(self.application_path))
            os.system('start {0}\\Setting.exe'.format(self.application_path))
            print("WellDone")
        else:
            print('NothingNew')

if __name__ == "__main__":
    res = Update()
    print(res.云端版本())
    #print(res)
    #o = input("退出")
    #res.RepSW()
    print("更新完毕")