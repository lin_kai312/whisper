#这是部署工具
import time, os, ctypes, sys, queue

from numpy import empty
from Function import Function
import argparse#用来获取命令参数
import threading
import base
import sys
from IOTMqtt import IOTControl

class Main:
    def __init__(self):
        super(Main, self).__init__()
        self.版本号 = base.基础().加载配置文档()['软件']['版本号']
        print("INFO SYSTEM 00: 当前版本号: ",self.版本号)

    def 网络代理设置(self):#程序初始化
        import urllib.request#导入网络库
        # 代理IP对象
        print("INFO START 01: ","设置网络代理")
        proxy = urllib.request.ProxyHandler({})
        # 建立opener对象，给该opener添加代理IP
        # 参数2：设置为HTTPHandler【固定不可改】
        opener = urllib.request.build_opener(proxy, urllib.request.HTTPHandler)
        # 安装为全局opener()
        print("INFO START 02: ","设置全局网络代理")
        urllib.request.install_opener(opener)

    def 命令行设置(self):#程序初始化
        #创建命令行获取设置
        运行要求 = sys.argv[1:]
        # print(sys.argv)
        # 在有控制台的情况下返回软件版本
        # 立刻检查更新，仅检查更新，不进行其他功能
        # 报告这是使用命令重启程序以获取管理员权限
        运行要求.append('start')
        if 'version' in 运行要求:
            print(self.版本号)
        if 运行要求 is empty:
            print("程序路径",sys.executable)
            os.system("start %s start"%sys.executable)
        elif 'upadte' in 运行要求:
            self.运行准备()
            if not self.运行功能.检查网络():
                self.运行功能.更新程序()
        elif 'start' in 运行要求:
            print("主程序运行 ")
            self.运行准备()
            if not self.运行功能.检查网络():
                命令队列 = self.MQTT.命令队列
                命令队列.put(("返回信息","{0} {1}".format(sys.argv[0],self.版本号)))
                print("功能运行")
                while True:
                    self.运行功能.main_loop(命令队列,1)

                
    def 运行准备(self):
        等待时长 = 1  # 秒为单位
        for 等待 in range(0,等待时长):
            time.sleep(1)
        # 创建启动项
        base.基础().设置自启动(base.基础().加载配置文档()['软件']['启动器指令'])
        self.MQTT = IOTControl(base.基础().加载配置文档())
        #self.MQTT.re_connect()
        self.运行功能 = Function(self.MQTT)


if __name__ == '__main__':
    main = Main()
    #main.InitializeAu()
    main.命令行设置()
    # 等待 = input()
