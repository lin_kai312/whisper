说明
readme_zh-cn
# 微言 Whisper

## 项目背景
记一位可怜人可怜的故事，开发了本项目
感谢[Gitee](https://gitee.com)提供帮助

## 项目介绍
**微言** 本意就是微小的话语，复指指令、代码。微言是发声，是沟通
包含微言Hope以及其他项目，将会在未来更新。
*具体可再阅读分支下的说明文件*

## 日志
仓库的建立是由于WhisperHope的开发。
Whisper第一阶段的开发：第一句代码写于2022年4月27日，最后一个满足基本需求的完整的可执行程序编译结束在2022年5月9日，最后一次测试在10号


## 仓库程序介绍
### 微言Hope  WhisperHope
#### 内容
正如名称所言，这是一个希望项目，基于python以及适用系统的语言
主体为一套程序，主程序，更新程序update，部署工具Setting。
主程序包含：Base包，MQTT包，Function包，主程序包，外壳
还有：视频播放器-ClearWater



## 作者信息



